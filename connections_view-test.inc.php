<?php
/**
 * WSRFC - Connections View (Test)
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// check for connection
if($connection_obj->id){
 // execute a test call
 $wsrfc=$connection_obj->test();
 // get result
 $date_dd=$wsrfc['result']['TAG']." ".$wsrfc['result']['UHRZEIT'];
 // build connection description list
 $test_dl=new strDescriptionList("br","dl-horizontal");
 $test_dl->addElement(api_text("connections_view-test_modal-dt-date"),$date_dd);
 $test_dl->addElement(api_text("connections_view-test_modal-dt-debug"),api_tag("pre",print_r($wsrfc,true)));
 // build connection view modal window
 $test_modal=new strModal(api_text("connections_view-test_modal-title",$connection_obj->name),null,"connections_view-test_modal");
 $test_modal->setBody($test_dl->render());
 $test_modal->setSize("large");
 // add modal to application
 $app->addModal($test_modal);
 // jQuery scripts
 $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_connections_view-test_modal').modal('show');});");
}
?>