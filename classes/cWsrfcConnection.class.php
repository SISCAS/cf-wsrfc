<?php
/**
 * WSRFC - Connection
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

/**
 * WSRFC Connection class
 */
class cWsrfcConnection{

 /** Properties */
 protected $id;
 protected $code;
 protected $name;
 protected $url;
 protected $token;
 protected $username;
 protected $password;
 protected $default;
 protected $addTimestamp;
 protected $addFkUser;
 protected $updTimestamp;
 protected $updFkUser;
 protected $deleted;

 /**
  * Debug
  *
  * @return object Connection object
  */
 public function debug(){return $this;}

 /**
  * Connection class
  *
  * @param mixed $connection Connection object, code or ID
  */
 public function __construct($connection){
  // get object
  if($connection=="default"){$connection=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `wsrfc__connections` WHERE `default`='1'");}
  if(is_numeric($connection)){$connection=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `wsrfc__connections` WHERE `id`='".$connection."'");}
  if(is_string($connection)){$connection=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `wsrfc__connections` WHERE `code`='".$connection."'");}
  if(!$connection->id){return false;}
  // set properties
  $this->id=(int)$connection->id;
  $this->code=stripslashes($connection->code);
  $this->name=stripslashes($connection->name);
  $this->url=stripslashes($connection->url);
  $this->token=stripslashes($connection->token);
  $this->username=stripslashes($connection->username);
  $this->password=stripslashes($connection->password);
  $this->default=(bool)$connection->default;
  $this->addTimestamp=(int)$connection->addTimestamp;
  $this->addFkUser=(int)$connection->addFkUser;
  $this->updTimestamp=(int)$connection->updTimestamp;
  $this->updFkUser=(int)$connection->updFkUser;
  $this->deleted=(int)$connection->deleted;
 }

 /**
  * Get
  *
  * @param string $property Property name
  * @return string Property value
  */
 public function __get($property){return $this->$property;}

 /**
  * Get Events
  */
 public function getEvents($debug=false){
  // definitions
  $events_array=array();
  // make query where
  if(DEBUG || $debug){$query_where="1";}else{$query_where="`level`<>'debug'";}
  // get connection events
  $events_results=$GLOBALS['database']->queryObjects("SELECT * FROM `wsrfc__events` WHERE ".$query_where." AND `fkConnection`='".$this->id."' ORDER BY `timestamp` DESC");
  foreach($events_results as $event){$events_array[$event->id]=new cEvent($event,"wsrfc");}
  // return
  return $events_array;
 }

 /**
  * Execute Remote Function Call
  *
  * @param type $function
  * @param type $input
  * @param type $username
  * @param type $password
  * @param type $verbose
  *
  * @return array WSRFC result
  */
 public function execute($function,$input=null,$username=null,$password=null,$verbose=false,$debug=false){
  // check deleted
  if($this->disabled){return false;}
  // check parameters
  if(!$function){return false;}
  if(!is_array($input)){$input=array();}
  // build post data
  $post_data=array(
   "token"=>$this->token,
   "username"=>(strlen($username)?$username:$this->username),
   "password"=>(strlen($password)?$password:$this->password),
   "function"=>$function,
   "input"=>$input,
   "verbose"=>($verbose?1:0),
   "debug"=>($debug?1:0)
  );
  // debug
  if($debug){api_dump($post_data);}
  // build http options
  $options=array(
   "http"=>array(
    "header"=>"Content-type: application/x-www-form-urlencoded\r\n",
    "method"=>"POST",
    "content"=>http_build_query($post_data),
    "timeout"=>3600
   ),
  );
  // make stram context
  $context=stream_context_create($options);
  // get from http
  $response=file_get_contents($this->url."rfc.php",false,$context);
  // decode result
  $return=json_decode($response,true);
  // return
  return $return;
 }

 /**
  * Test Connection
  *
  * @return array WSRFC result
  */
 public function test(){
  // execute test function to get sap timestamp
  return $this->execute("BDL_GET_CENTRAL_TIMESTAMP",null,null,null,true);
 }

}
?>