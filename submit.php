<?php
/**
 * WSRFC - Submit
 *
 * @package Coordinator\Modules\WSRFC
 * @connection Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// check for actions
if(!defined('ACTION')){die("ERROR EXECUTING SCRIPT: The action was not defined");}
// switch action
switch(ACTION){
 // connections
 case "connection_save":connection_save();break;
 case "connection_default":connection_default();break;
 case "connection_delete":connection_deleted(true);break;
 case "connection_undelete":connection_deleted(false);break;
 case "connection_remove":connection_remove();break;
 // default
 default:
  api_alerts_add(api_text("alert_submitFunctionNotFound",array(MODULE,SCRIPT,ACTION)),"danger");
  api_redirect("?mod=".MODULE);
}

/**
 * Connection Save
 */
function connection_save(){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get connection object
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 api_dump($connection_obj,"connection object");
 // build connection query object
 $connection_qobj=new stdClass();
 $connection_qobj->id=$connection_obj->id;
 $connection_qobj->code=addslashes($_REQUEST['code']);
 $connection_qobj->name=addslashes($_REQUEST['name']);
 $connection_qobj->url=addslashes($_REQUEST['url']);
 $connection_qobj->token=addslashes($_REQUEST['token']);
 $connection_qobj->username=addslashes($_REQUEST['username']);
 $connection_qobj->password=addslashes($_REQUEST['password']);
 // check url
 if(substr($connection_qobj->url,-1)!="/"){$connection_qobj->url.="/";}
 // check for update
 if($connection_obj->id){
  // update connection
  $connection_qobj->updTimestamp=time();
  $connection_qobj->updFkUser=$GLOBALS['session']->user->id;
  api_dump($connection_qobj,"connection query object");
  // execute query
  $GLOBALS['database']->queryUpdate("wsrfc__connections",$connection_qobj);
  // log event
  api_wsrfc_event_save($connection_qobj,"connectionUpdated");
  // alert
  api_alerts_add(api_text("wsrfc_alert-connectionUpdated"),"success");
 }else{
  // insert connection
  $connection_qobj->addTimestamp=time();
  $connection_qobj->addFkUser=$GLOBALS['session']->user->id;
  api_dump($connection_qobj,"connection query object");
  // execute query
  $connection_qobj->id=$GLOBALS['database']->queryInsert("wsrfc__connections",$connection_qobj);
  // log event
  api_wsrfc_event_save($connection_qobj,"connectionCreated");
  // alert
  api_alerts_add(api_text("wsrfc_alert-connectionCreated"),"success");
 }
 // redirect
 api_redirect("?mod=".MODULE."&scr=".api_return_script("connections_view")."&idConnection=".$connection_qobj->id);
}

/**
 * Connection Default
 */
function connection_default(){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get connection object
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 api_dump($connection_obj,"connection object");
 // check object
 if(!$connection_obj->id){api_alerts_add(api_text("wsrfc_alert-connectionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=connections_list");}
 // remove previous default
 $GLOBALS['database']->queryExecute("UPDATE `wsrfc__connections` SET `default`='0'");
 // build connection query objects
 $connection_qobj=new stdClass();
 $connection_qobj->id=$connection_obj->id;
 $connection_qobj->default=true;
 $connection_qobj->updTimestamp=time();
 $connection_qobj->updFkUser=$GLOBALS['session']->user->id;
 api_dump($connection_obj,"connection object");
 // update connection
 $GLOBALS['database']->queryUpdate("wsrfc__connections",$connection_qobj);
 // log event
 api_wsrfc_event_save($connection_obj,"connectionDefaulted");
 // redirect
 api_alerts_add(api_text("wsrfc_alert-connectionDefaulted"),"success");
 api_redirect("?mod=".MODULE."&scr=".api_return_script("connections_view")."&idConnection=".$connection_obj->id);
}

/**
 * Connection Deleted
 *
 * @param boolean $deleted Deleted or Undeleted
 */
function connection_deleted($deleted){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get connection object
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 api_dump($connection_obj,"connection object");
 // check object
 if(!$connection_obj->id){api_alerts_add(api_text("wsrfc_alert-connectionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=connections_list");}
 // build connection query objects
 $connection_qobj=new stdClass();
 $connection_qobj->id=$connection_obj->id;
 $connection_qobj->deleted=($deleted?1:0);
 $connection_qobj->updTimestamp=time();
 $connection_qobj->updFkUser=$GLOBALS['session']->user->id;
 api_dump($connection_obj,"connection object");
 // update connection
 $GLOBALS['database']->queryUpdate("wsrfc__connections",$connection_qobj);
 // make action
 if($deleted){$action="connectionDeleted";}else{$action="connectionUndeleted";}
 // log event
 api_wsrfc_event_save($connection_obj,$action,"warning");
 // redirect
 api_alerts_add(api_text("wsrfc_alert-".$action),"warning");
 api_redirect("?mod=".MODULE."&scr=".api_return_script("connections_list")."&idConnection=".$connection_obj->id);
}

/**
 * Connection Remove
 */
function connection_remove(){
 api_dump($_REQUEST,"_REQUEST");
 // check authorizations
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get connection object
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 api_dump($connection_obj,"connection object");
 // check object
 if(!$connection_obj->id){api_alerts_add(api_text("wsrfc_alert-connectionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=connections_list");}
 // delete connection
 $GLOBALS['database']->queryDelete("wsrfc__connections",$connection_obj->id);
 /** @tip save event or sendamail ? */
 // redirect
 api_alerts_add(api_text("wsrfc_alert-connectionRemoved"),"warning");
 api_redirect("?mod=".MODULE."&scr=".api_return_script("connections_list"));
}

?>