<?php
/**
 * WSRFC
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 $module_name="wsrfc";
 $module_repository_url="https://bitbucket.org/SISCAS/cf-wsrfc/";
 $module_repository_version_url="https://bitbucket.org/SISCAS/cf-wsrfc/raw/master/VERSION.txt";
 $module_required_modules=array();
?>