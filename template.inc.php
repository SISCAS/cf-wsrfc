<?php
/**
 * WSRFC - Template
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // build application
 $app=new strApplication();
 // build nav object
 $nav=new strNav("nav-tabs");
 $nav->setTitle(api_text("wsrfc"));
 // dashboard
 $nav->addItem(api_icon("fa-th-large",null,"hidden-link"),"?mod=".MODULE."&scr=dashboard");
 // connections
 if(substr(SCRIPT,0,11)=="connections"){
  $nav->addItem(api_text("nav-connections-list"),"?mod=".MODULE."&scr=connections_list",(api_checkAuthorization("wsrfc-manage")));
  // operations
  if($connection_obj->id && in_array(SCRIPT,array("connections_view","connections_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-connections-operations-test"),"?mod=".MODULE."&scr=connections_view&act=connection_test&idConnection=".$connection_obj->id,(api_checkAuthorization("wsrfc-manage")));
   $nav->addSubItem(api_text("nav-connections-operations-edit"),"?mod=".MODULE."&scr=connections_edit&idConnection=".$connection_obj->id,(api_checkAuthorization("wsrfc-manage")));
   if(!$connection_obj->default){$nav->addSubItem(api_text("nav-connections-operations-default"),"?mod=".MODULE."&scr=submit&act=connection_default&idConnection=".$connection_obj->id,(api_checkAuthorization("wsrfc-manage")));}
  }else{$nav->addItem(api_text("nav-connections-add"),"?mod=".MODULE."&scr=connections_edit",(api_checkAuthorization("wsrfc-manage")));}
 }
 // add nav to html
 $app->addContent($nav->render(false));
?>