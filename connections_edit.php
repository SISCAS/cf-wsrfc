<?php
/**
 * WSRFC - Connections Edit
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get objects
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($connection_obj->id?api_text("connections_edit",$connection_obj->name):api_text("connections_edit-add")));
 // build connection form
 $form=new strForm("?mod=".MODULE."&scr=submit&act=connection_save&idConnection=".$connection_obj->id."&return_scr=".$_REQUEST['return_scr'],"POST",null,null,"connections_edit");
 $form->addField("text","code",api_text("connections_edit-ff-code"),$connection_obj->code,api_text("connections_edit-ff-code-placeholder"),null,null,null,"required");
 $form->addField("text","name",api_text("connections_edit-ff-name"),$connection_obj->name,api_text("connections_edit-ff-name-placeholder"),null,null,null,"required");
 $form->addField("text","url",api_text("connections_edit-ff-url"),$connection_obj->url,api_text("connections_edit-ff-url-placeholder"),null,null,null,"required");
 $form->addField("text","token",api_text("connections_edit-ff-token"),$connection_obj->token,api_text("connections_edit-ff-token-placeholder"),null,null,null,"required");
 $form->addField("text","username",api_text("connections_edit-ff-username"),$connection_obj->username,api_text("connections_edit-ff-username-placeholder"));
 $form->addField("text","password",api_text("connections_edit-ff-password"),$connection_obj->password,api_text("connections_edit-ff-password-placeholder"));
 // controls
 $form->addControl("submit",api_text("form-fc-submit"));
 if($connection_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=".api_return_script("connections_view")."&idConnection=".$connection_obj->id);
  if(!$connection_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),"?mod=".MODULE."&scr=submit&act=connection_delete&idConnection=".$connection_obj->id,"btn-danger",api_text("connections_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),"?mod=".MODULE."&scr=submit&act=connection_undelete&idConnection=".$connection_obj->id,"btn-warning");
   $form->addControl("button",api_text("form-fc-remove"),"?mod=".MODULE."&scr=submit&act=connection_remove&idConnection=".$connection_obj->id,"btn-danger",api_text("connections_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=connections_list");}
 // jQuery scripts
 $app->addScript("/* Select2 Code */\n$(function(){\$('select[name=\"code\"]').select2({width:'100%'});});"); /*,dropdownParent:$('#modal_companies_view_operators_add_modal')*/
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($connection_obj,"connection");
?>