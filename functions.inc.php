<?php
/**
 * WSRFC Functions
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

// include classes
require_once(DIR."modules/wsrfc/classes/cWsrfcConnection.class.php");

/**
 * WSRFC - Event Save
 *
 * @param object|integer $connection Connection object or ID
 * @param string $event Event to log
 * @param string $level Level [information|warning|error|debug]
 * @param string $note Note
 * @return boolean true or false
 */
function api_wsrfc_event_save($connection,$event,$level="information",$note=null){ /* @todo valutare se fare api */
 // check parameters
 $connection_obj=new cWsrfcConnection($connection);
 //if(!($connection_obj instanceof cWsrfcConnection)){return false;}
 if(!$connection_obj->id){return false;}
 if(!$event){return false;}
 // build event query objects
 $event_qobj=new stdClass();
 $event_qobj->fkConnection=$connection_obj->id;
 if($GLOBALS['session']->user->id){$event_qobj->fkUser=$GLOBALS['session']->user->id;}
 $event_qobj->timestamp=time();
 $event_qobj->event=$event;
 $event_qobj->level=$level;
 $event_qobj->note=$note;
 // debug
 api_dump($event_qobj,"event query object");
 // insert event
 $event_qobj->id=$GLOBALS['database']->queryInsert("wsrfc__events",$event_qobj);
 // check and return
 if($event_qobj->id){return true;}
 else{return false;}
}

/**
 * WSRFC - Date to Timestamp
 *
 * @param string $date SAP date in format YYYYMMDD
 * @param string $time SAP time in format HHMMSS
 * @return integer Timestamp
 */
function api_wsrfc_dateToTimestamp($date,$time=null){
 // check parameters
 if(strlen($date)!=8){return false;}
 // make datetime
 $v_datetime=substr($date,0,4)."-".substr($date,4,2)."-".substr($date,6,2);
 if($time){$v_datetime.=" ".substr($time,0,4)."-".substr($time,2,2)."-".$time($date,4,2);}
 // convert and return
 return api_timestamp($v_datetime);
}

/**
 * WSRFC - Timestamp to Date
 *
 * @param integer $timestamp Timestamp
 * @return string SAP date in format YYYYMMDD
 */
function api_wsrfc_timestampToDate($timestamp){
 // check parameters
 if(!is_int($timestamp)){return false;}
 // convert and return
 return api_timestamp_format($timestamp,"Ymd");
}

/**
 * WSRFC - Timestamp to Time
 *
 * @param string $timestamp Timestamp
 * @return string SAP time in format HHMMSS
 */
function api_wsrfc_timestampToTime($timestamp){
 // check parameters
 if(!is_int($timestamp)){return false;}
 // convert and return
 return api_timestamp_format($timestamp,"His");
}

?>