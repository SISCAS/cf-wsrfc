<?php
/**
 * WSRFC - Connections View
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("wsrfc-manage","dashboard");
 // get objects
 $connection_obj=new cWsrfcConnection($_REQUEST['idConnection']);
 // check objects
 if(!$connection_obj->id){api_alerts_add(api_text("wsrfc_alert-connectionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=connections_list");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("connections_view",$connection_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build connection description list
 $connection_dl=new strDescriptionList("br","dl-horizontal");
 $connection_dl->addElement(api_text("connections_view-dt-code"),($connection_obj->default?api_icon("fa-star",api_text("connections_view-dd-default"))." ":null).api_tag("samp",$connection_obj->code));
 $connection_dl->addElement(api_text("connections_view-dt-name"),$connection_obj->name);
 // build informations description list
 $information_dl=new strDescriptionList("br","dl-horizontal");
 $information_dl->addElement(api_text("connections_view-dt-url"),api_link($connection_obj->url,$connection_obj->url,null,null,false,null,null,null,"_blank"));
 $information_dl->addElement(api_text("connections_view-dt-token"),api_tag("samp",$connection_obj->token));
 $information_dl->addElement(api_text("connections_view-dt-username"),$connection_obj->username);
 $information_dl->addElement(api_text("connections_view-dt-password"),($_REQUEST['password']?$connection_obj->password:api_link("?mod=".MODULE."&scr=connections_view&tab=informations&password=1&idConnection=".$connection_obj->id,str_repeat("*",strlen($connection_obj->password)))));
 // build tabs
 $tab=new strTab();
 $tab->addItem(api_icon("fa-bookmark")." ".api_text("connections_view-tab-informations"),$information_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("connections_view-tab-events"),api_events_table($connection_obj->getEvents())->render(),("events"==TAB?"active":null));
 // check for test action
 if(ACTION=="connection_test"){require_once(MODULE_PATH."connections_view-test.inc.php");}
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($connection_dl->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($connection_obj,"connection");
?>