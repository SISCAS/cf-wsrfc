<?php
/**
 * WSRFC - Connections List
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("wsrfc-manage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $users_array=array();
 // set application title
 $app->setTitle(api_text("connections_list"));
 // definitions
 $connections_array=array();
 // build filter
 $filter=new strFilter();
 $filter->addSearch(array("code","name","url","token"));
 // build query object
 $query=new cQuery("wsrfc__connections",$filter->getQueryWhere());
 $query->addQueryOrderField("code");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$connections_array[$result_f->id]=new cWsrfcConnection($result_f);}
 // build table
 $table=new strTable(api_text("connections_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("connections_list-th-code"),"nowrap");
 //$table->addHeader("&nbsp;",null,16);
 $table->addHeader(api_text("connections_list-th-token"),"nowrap");
 $table->addHeader(api_text("connections_list-th-name"),null,"100%");
 $table->addHeader("&nbsp;",null,16);
 // cycle all connections
 foreach($connections_array as $connection_obj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement("?mod=".MODULE."&scr=connections_edit&idConnection=".$connection_obj->id."&return_scr=connections_list","fa-pencil",api_text("connections_list-td-edit"));
  if($connection_obj->deleted){$ob->addElement("?mod=".MODULE."&scr=submit&act=connection_undelete&idConnection=".$connection_obj->id,"fa-trash-o",api_text("connections_list-td-undelete"),true,api_text("connections_list-td-undelete-confirm"));}
  else{$ob->addElement("?mod=".MODULE."&scr=submit&act=connection_delete&idConnection=".$connection_obj->id,"fa-trash",api_text("connections_list-td-delete"),true,api_text("connections_list-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($connection_obj->id==$_REQUEST['idConnection']){$tr_class_array[]="currentrow";}
  if($connection_obj->default){$tr_class_array[]="success";}
  if($connection_obj->deleted){$tr_class_array[]="deleted";}
  // build connection row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction("?mod=".MODULE."&scr=connections_view&idConnection=".$connection_obj->id,"fa-search",api_text("connections_list-td-view"));
  $table->addRowField(api_tag("samp",$connection_obj->code),"nowrap");
  $table->addRowField(api_tag("samp",$connection_obj->token),"nowrap");
  $table->addRowField($connection_obj->name,"truncate-ellipsis");
  $table->addRowField($ob->render(),"text-right");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");
 api_dump($query->getQuerySQL(),"query sql");
?>