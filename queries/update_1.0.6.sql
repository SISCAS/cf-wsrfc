--
-- WSRFC - Update (1.0.39)
--
-- @package Coordinator\Modules\WSRFC
-- @company Cogne Acciai Speciali s.p.a
-- @authors Manuel Zavatta <manuel.zavatta@cogne.com>
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `wsrfc__connections`
--

ALTER TABLE `wsrfc__connections` ADD `default` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `password`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
