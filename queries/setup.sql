--
-- Setup module WSRFC
--
-- Version 1.0.0
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `wsrfc__connections`
--

CREATE TABLE IF NOT EXISTS `wsrfc__connections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wsrfc__events`
--

CREATE TABLE IF NOT EXISTS `wsrfc__events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkConnection` int(11) unsigned NOT NULL,
  `fkUser` int(11) unsigned DEFAULT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkConnection` (`fkConnection`),
  CONSTRAINT `wsrfc__events_ibfk_1` FOREIGN KEY (`fkConnection`) REFERENCES `wsrfc__connections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('wsrfc-manage','wsrfc',1);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;