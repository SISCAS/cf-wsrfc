<?php
/**
 * WSRFC - Dashboard
 *
 * @package Coordinator\Modules\WSRFC
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text(MODULE));
 // build dashboard object
 $dashboard=new strDashboard();
 $dashboard->addTile("?mod=".MODULE."&scr=connections_list",api_text("dashboard-connections-list"),api_text("dashboard-connections-list-description"),(api_checkAuthorization("wsrfc-manage")),"1x1","fa-book");
 $dashboard->addTile("?mod=".MODULE."&scr=connections_edit",api_text("dashboard-connections-add"),api_text("dashboard-connections-add-description"),(api_checkAuthorization("wsrfc-manage")),"1x1","fa-file-text-o");
  // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dashboard->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
?>